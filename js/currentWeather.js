/* let forecastLink = 'http://api.openweathermap.org/data/2.5/forecast?appid=077b06b6853fcce3c35d2a460809ad46&units=metric'; */
let currentWeatherLink = 'https://api.openweathermap.org/data/2.5/weather?appid=077b06b6853fcce3c35d2a460809ad46&units=metric';
const weatherContainer = document.querySelector('.weather-container');
const curveUp = document.querySelector('.curve_up');
const curveDown = document.querySelector('.curve_down');
const forecastArea = document.querySelector('.forecast');
const textFiledsArea = document.querySelector('.text-fields');

/*Object with weather conditions codes*/

const weatherConditions = {
    rainy: [300, 301, 302, 310, 311, 312, 313, 314, 321, 500, 501, 502, 503, 504, 511, 520, 521, 522, 531, 701],
    snowy: [600, 601, 602, 611, 612, 613, 615, 616, 620, 621, 622],
    sunny: [800],
    partlyCloudy: [801, 802],
    cloudy: [803, 804],
    foggy: [741, 711, 701, 721, 731, 751, 761, 762, 771, 781],
    storm: [200, 201, 202],
    thunder: [210, 211, 212, 221, 230, 231, 232],
};

/*Object to implement to a city name from a user's search query*/

let storage = {
    city: 'Moscow',
};

/*Getting data from API*/

const fetchData = async () => {
    const result = await fetch(`${currentWeatherLink}&q=${storage.city}`);
    const data = await result.json();
    console.log(data);

    let name = data.name;

    let {
        main: {
            temp: temperature,
            feels_like: feelsLikeTemp,
            humidity,
        },

        weather: {
            [0]: condition,
            [0]: id,
        },

        timezone,

        dt: dateTime,
    } = data;

    function getCurrentTime(dateTime) {
        let date = new Date(dateTime * 1000);
        return date.getHours() + timezone / 3600 - 3;
    }

    console.log(getCurrentTime(dateTime));

    let weatherDescription = condition.main;
    let conditionId = id.id;


    /*Condition to change the night/day layout */

    let isDay;
    /*  function isDayNow() {
         if (getCurrentTime(dateTime) > 4 || getCurrentTime(dateTime) <= 17) {
             isDay = 'yes';
         } else {
             isDay = 'no';
         }
     } */
    getCurrentTime(dateTime) > 4 ? isDay = 'yes' : isDay = 'no';
    getCurrentTime(dateTime) <= 17 ? isDay = 'yes' : isDay = 'no';



    console.log(isDay);

    if (isDay === 'yes') {
        weatherContainer.classList.add('isDay');
        curveUp.classList.add('isDay');
        curveDown.classList.add('isDay');
        forecastArea.classList.add('isDay');
        textFiledsArea.classList.add('isDay');
    } else {
        weatherContainer.classList.remove('isDay');
        curveUp.classList.remove('isDay');
        curveDown.classList.remove('isDay');
        forecastArea.classList.remove('isDay');
        textFiledsArea.classList.remove('isDay');
    }

    /*Function to get an object key, that includes the weather condition code the API has returned*/

    function findByCode(code) {
        return Object.keys(weatherConditions).find((key) => {
            return weatherConditions[key].includes(code);
        })
    }

    /*Choosing a picture according to the Object key*/

    let img = document.querySelector('img');

    switch (findByCode(conditionId)) {
        case 'rainy':
            img.src = './img/rain.png';
            break;
        case 'snowy':
            img.src = './img/snow.png';
            break;
        case 'sunny':
            img.src = './img/sun.png';
            break;
        case 'partlyCloudy':
            img.src = './img/partly-cloudy.png';
            break;
        case 'cloudy':
            img.src = './img/clouds.png';
            break;
        case 'foggy':
            img.src = './img/fog.png';
            break;
        case 'storm':
            img.src = './img/storm.png';
            break;
        case 'thunder':
            img.src = './img/thunder.png';
            break;
        default:
            img.src = './img/weather-news.png'
            break;
    }

    /*Getting all of the elements for data implementation*/

    let city = document.querySelector('.city-name');
    let loadedTemperature = document.querySelector('.temperature');
    let feelsLike = document.querySelector('.feels-like');
    let foundHumidity = document.querySelector('.humidity');
    let description = document.querySelector('.description');

    /*Adding the data to the user interface*/

    city.innerHTML = name;
    loadedTemperature.innerHTML = `${Math.round(temperature)}°`;
    feelsLike.innerHTML = `Feels like: ${Math.round(feelsLikeTemp)}°`;
    foundHumidity.innerHTML = `Humidity: ${humidity}%`;
    description.innerHTML = weatherDescription;

}

fetchData();
